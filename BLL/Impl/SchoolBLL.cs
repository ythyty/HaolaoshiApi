using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class SchoolBll : BaseBll<School>, ISchoolBll
    {
        public SchoolBll(ISchoolDAL dal):base(dal)
        {
        }
    }
}
