/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class NoticeBll : BaseBll<Notice>, INoticeBll
    {
        public NoticeBll(INoticeDAL dal):base(dal)
        {
        }
    }
}
