﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
using System.Linq;
using Bll.Dto;
using DAL.Extension;
using AutoMapper;

namespace Bll.Impl
{
    public class UsualScoreBll : BaseBll<UsualScore>, IUsualScoreBll
    {
        public IStudentBll studentBll { get; set; }
        public IClasssBll classsBll { get; set; }
        public IUsualScoreLogBll usualScoreLogBll { get; set; }
        public IUsualScoreItemBll usualScoreItemBll { get; set; }
        public ISchoolConfigBll schoolConfigBll { get; set; }
        public Mapper Mapper { get; set; }
        public UsualScoreBll(IUsualScoreDAL dal) : base(dal)
        {
        }
        public IEnumerable<UsualScoreDto> SelectAllOrInit(int CourseId, int ClasssId, int schoolId, int userId)
        {
            //IEnumerable<UsualScore> objs = this.SelectAll(o => o.CourseId == CourseId && o.Student.ClasssId == ClasssId);
            var objs = base.Query().Where(o => o.CourseId == CourseId && o.Student.ClasssId == ClasssId).Select<UsualScoreDto>().ToList();
            if (!objs.GetEnumerator().MoveNext())//没有记录 初始化成绩信息
            {
                IEnumerable<Student> students = studentBll.SelectAll(o => o.ClasssId == ClasssId);//查询学生
                //获取学校配置参数
                SchoolConfig schoolConfig = schoolConfigBll.SelectOneBySchoolOrInit(schoolId);
                List<UsualScore> usualScores = new List<UsualScore>();
                foreach (Student s in students)
                {
                    //int? schoolId = classsBll.SelectOne(s.ClasssId)?.SchoolId;//获取学校
                    //初始化学生成绩
                    usualScores.Add(new UsualScore() { SchoolId = s.Classs?.SchoolId, StudentId = s.Id, Student = s, CourseId = CourseId, Score = schoolConfig.UsualScoreScore, Sys = false });
                }
                this.DbContext().Database.BeginTransaction();//开启事务
                bool ret = this.Add(usualScores);
                this.DbContext().Database.CommitTransaction();
                return Mapper.Map<List<UsualScore>, List<UsualScoreDto>>(usualScores);//映射轉換
            }
            return objs;
        }
        public UsualScore SelectOneOrInit(int StudentId, int CourseId, int? SchoolId)
        {
            //查找平时成绩
            UsualScore usualScore = SelectOne(o => o.StudentId == StudentId && o.CourseId == CourseId);
            if (usualScore == null)//没有记录 初始化成绩信息
            {
                Student s = studentBll.SelectOne(o => o.Id == StudentId);//查询学生
                //获取学校配置参数
                SchoolConfig schoolConfig = schoolConfigBll.SelectOneBySchoolOrInit(SchoolId.GetValueOrDefault());
                //初始化学生成绩
                usualScore = new UsualScore() { SchoolId = SchoolId, StudentId = s.Id, Student = s, CourseId = CourseId, Score = schoolConfig.UsualScoreScore, Sys = false };
                bool ret = this.Add(usualScore);
                return usualScore;
            }
            return usualScore;
        }
    }
}
