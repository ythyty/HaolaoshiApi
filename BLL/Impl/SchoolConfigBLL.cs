using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class SchoolConfigBll : BaseBll<SchoolConfig>, ISchoolConfigBll
    {
        public SchoolConfigBll(ISchoolConfigDAL dal) : base(dal)
        {
        }
      
        public SchoolConfig SelectOneBySchoolOrInit(int schoolId)
        {
            SchoolConfig obj = null;
            if (schoolId == 0)//不是以学校教务身份登录即以系统管理员身份登录
            {
                //查询针对所有学校的配置
                obj = this.SelectOne(o => o.SchoolId == null);
                if (obj == null)//没有默认配置初始默认配置
                {
                    obj = new SchoolConfig() { SchoolId = null };
                    this.Add(obj);
                }
            }
            else
            {
                obj = this.SelectOne(o => o.SchoolId == schoolId);
                if (obj == null)//没有默认配置初始默认配置
                {
                    obj = new SchoolConfig() { SchoolId = schoolId };
                    this.Add(obj);
                }
            }
            return obj;
        }
    }
}
