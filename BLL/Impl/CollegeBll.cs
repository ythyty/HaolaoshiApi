using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class CollegeBll : BaseBll<College>, ICollegeBll
    {
        public CollegeBll(ICollegeDAL dal):base(dal)
        {
        }
    }
}
