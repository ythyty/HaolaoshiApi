﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Extension;
namespace Bll.Dto
{
    //平时成绩
    [Serializable]
    public class UsualScoreDto
    {
        public int Id { get; set; }
        public int? SchoolId { get; set; }
        [Display(Name = "学校名称")]
        [FromModel("School.Name")]
        public string SchoolName { get; set; }
        public int? StudentId { get; set; }
        [FromModel("Student.Realname")]
        public string StudentName { get; set; }
        public int? CourseId { get; set; }    
        [Display(Name = "课程名称")]
        [FromModel("Course.Name")]
        public string CourseName { get; set; }     
        [Display(Name = "分数")]
        public double Score { get; set; }
        [Display(Name = "备注")]
        public string Intro { get; set; }
    }
}