﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Model;
using Bll.Dto;
namespace BLL.AutoMapper
{

    /// <summary>
    /// 配置构造函数，用来创建关系映射.用来配置所有的映射关系。
    /// </summary>
    public class BllMappingProfile : Profile
    {
        public BllMappingProfile()
        {
            CreateMap<UsualScore, UsualScoreDto>();
                //属性匹配，匹配源类中WorkEvent.Date到EventDate
                      //  .ForMember(dest => dest.EventDate, opt => opt.MapFrom(src => src.WorkEvent.Date))
                        //.ForMember(dest => dest.SomeValue, opt => opt.Ignore())//忽略目标类中的属性
                        //.ForMember(dest => dest.TotalAmount, opt => opt.MapFrom(src => src.TotalAmount ?? 0))//复杂的匹配
                        //.ForMember(dest => dest.OrderDate, opt => opt.UserValue<DateTime>(DateTime.Now)); 固定值匹配;
        }
    }

}
