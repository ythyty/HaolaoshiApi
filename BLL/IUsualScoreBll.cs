﻿using System;
using System.Collections.Generic;
using System.Text;
using Bll.Dto;
using Model;
namespace Bll
{
   
    public interface IUsualScoreBll:IBaseBll<UsualScore>
    {
        /// <summary>
        /// 查询或初始化课程对应班级学生的平时成绩。没有则初始化。按学生编号升序排列
        /// </summary>
        /// <param name="CourseId"></param>
        /// <param name="ClasssId"></param>
        /// <param name="schoolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<UsualScoreDto> SelectAllOrInit(int CourseId, int ClasssId, int schoolId, int userId);
        /// <summary>
        ///  查询或初始化课程对应某个学生的平时成绩。没有则初始化
        /// </summary>
        /// <param name="StudentId">学生id</param>
        /// <param name="CourseId">课程id</param>
        /// <param name="SchoolId">学校id，如果为null，则选择默认学校设置</param>
        /// <returns></returns>
        UsualScore SelectOneOrInit(int StudentId,int CourseId,int? SchoolId); 
    }
}
