using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace Bll
{

    public interface ISchoolConfigBll : IBaseBll<SchoolConfig>
    {
        /// <summary>
        /// 获取当前学校的配置参数。
        /// 1、如果SchoolId为0，获取系统默认配置（即SchoolId为null的记录），不存在则添加默认记录
        /// 2、如果SchoolId不为0，获取指定学校的配置，不存在在添加某个学校的配置记录
        /// </summary>
        /// <param name="schoolId">学校编号</param>
        /// <param name="userId">用户编号，主要用于初始添加记录时候使用</param>
        /// <returns></returns>
        SchoolConfig SelectOneBySchoolOrInit(int schoolId);
        
    }
}
