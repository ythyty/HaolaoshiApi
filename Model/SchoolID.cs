﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model
{
    /// <summary>
    /// 统一
    /// </summary>
   public class SchoolID : ID,ISchool
    {
        public int? SchoolId { get; set; }
        [ForeignKey("SchoolId")]
        [Display(Name = "学校")]
        public virtual School School { get; set; }      
    }
}
