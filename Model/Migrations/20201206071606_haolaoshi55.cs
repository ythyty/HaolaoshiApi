﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi55 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SchoolConfig",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Added_time = table.Column<DateTime>(nullable: false),
                    Sys = table.Column<bool>(nullable: false),
                    Open = table.Column<bool>(nullable: false),
                    Passed = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: true),
                    SchoolId = table.Column<int>(nullable: true),
                    UsualScoreScore = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolConfig", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SchoolConfig_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SchoolConfig_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SchoolConfig_SchoolId",
                table: "SchoolConfig",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolConfig_UserId",
                table: "SchoolConfig",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SchoolConfig");
        }
    }
}
