﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi51 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interview_Company_CompanyId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Interview_CompanyId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Interview");

            migrationBuilder.AddColumn<int>(
                name: "PostId",
                table: "Interview",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Interview_PostId",
                table: "Interview",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_Post_PostId",
                table: "Interview",
                column: "PostId",
                principalTable: "Post",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interview_Post_PostId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Interview_PostId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "Interview");

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "Interview",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Interview_CompanyId",
                table: "Interview",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_Company_CompanyId",
                table: "Interview",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
