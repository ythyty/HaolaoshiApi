﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi61 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Student",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "RoleAuthority",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Role",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Res",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Notice",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Category",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "AuthorityRes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Authority",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Area",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_SchoolId",
                table: "User",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_SchoolId",
                table: "Student",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleAuthority_SchoolId",
                table: "RoleAuthority",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_SchoolId",
                table: "Role",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Res_SchoolId",
                table: "Res",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Notice_SchoolId",
                table: "Notice",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_SchoolId",
                table: "Category",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorityRes_SchoolId",
                table: "AuthorityRes",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Authority_SchoolId",
                table: "Authority",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Area_SchoolId",
                table: "Area",
                column: "SchoolId");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_School_SchoolId",
                table: "Area",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Authority_School_SchoolId",
                table: "Authority",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorityRes_School_SchoolId",
                table: "AuthorityRes",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Category_School_SchoolId",
                table: "Category",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notice_School_SchoolId",
                table: "Notice",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Res_School_SchoolId",
                table: "Res",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_School_SchoolId",
                table: "Role",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleAuthority_School_SchoolId",
                table: "RoleAuthority",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Student_School_SchoolId",
                table: "Student",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User_School_SchoolId",
                table: "User",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_School_SchoolId",
                table: "Area");

            migrationBuilder.DropForeignKey(
                name: "FK_Authority_School_SchoolId",
                table: "Authority");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorityRes_School_SchoolId",
                table: "AuthorityRes");

            migrationBuilder.DropForeignKey(
                name: "FK_Category_School_SchoolId",
                table: "Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Notice_School_SchoolId",
                table: "Notice");

            migrationBuilder.DropForeignKey(
                name: "FK_Res_School_SchoolId",
                table: "Res");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_School_SchoolId",
                table: "Role");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleAuthority_School_SchoolId",
                table: "RoleAuthority");

            migrationBuilder.DropForeignKey(
                name: "FK_Student_School_SchoolId",
                table: "Student");

            migrationBuilder.DropForeignKey(
                name: "FK_User_School_SchoolId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_User_SchoolId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_Student_SchoolId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_RoleAuthority_SchoolId",
                table: "RoleAuthority");

            migrationBuilder.DropIndex(
                name: "IX_Role_SchoolId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Res_SchoolId",
                table: "Res");

            migrationBuilder.DropIndex(
                name: "IX_Notice_SchoolId",
                table: "Notice");

            migrationBuilder.DropIndex(
                name: "IX_Category_SchoolId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_AuthorityRes_SchoolId",
                table: "AuthorityRes");

            migrationBuilder.DropIndex(
                name: "IX_Authority_SchoolId",
                table: "Authority");

            migrationBuilder.DropIndex(
                name: "IX_Area_SchoolId",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "User");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Notice");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Area");
        }
    }
}
