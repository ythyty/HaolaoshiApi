﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi34 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Open",
                table: "Course",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Course",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Sys",
                table: "Course",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Course_SchoolId",
                table: "Course",
                column: "SchoolId");

            migrationBuilder.AddForeignKey(
                name: "FK_Course_School_SchoolId",
                table: "Course",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_School_SchoolId",
                table: "Course");

            migrationBuilder.DropIndex(
                name: "IX_Course_SchoolId",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "Open",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "Sys",
                table: "Course");
        }
    }
}
