﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi37 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classs_Student_StudentId",
                table: "Classs");

            migrationBuilder.DropForeignKey(
                name: "FK_School_Student_StudentId",
                table: "School");

            migrationBuilder.DropForeignKey(
                name: "FK_School_Teacher_TeacherId",
                table: "School");

            migrationBuilder.DropIndex(
                name: "IX_School_StudentId",
                table: "School");

            migrationBuilder.DropIndex(
                name: "IX_School_TeacherId",
                table: "School");

            migrationBuilder.DropIndex(
                name: "IX_Classs_StudentId",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "School");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "School");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Classs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "School",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "School",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Classs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_School_StudentId",
                table: "School",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_School_TeacherId",
                table: "School",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Classs_StudentId",
                table: "Classs",
                column: "StudentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classs_Student_StudentId",
                table: "Classs",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_School_Student_StudentId",
                table: "School",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_School_Teacher_TeacherId",
                table: "School",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
