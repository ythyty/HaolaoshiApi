﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi64 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_User_Username",
                table: "User",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_Sn",
                table: "Teacher",
                column: "Sn",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_Username",
                table: "Teacher",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Student_Username",
                table: "Student",
                column: "Username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_User_Username",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_Teacher_Sn",
                table: "Teacher");

            migrationBuilder.DropIndex(
                name: "IX_Teacher_Username",
                table: "Teacher");

            migrationBuilder.DropIndex(
                name: "IX_Student_Username",
                table: "Student");
        }
    }
}
