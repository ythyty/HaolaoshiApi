﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi45 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "Course",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Course_ParentId",
                table: "Course",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Course_Course_ParentId",
                table: "Course",
                column: "ParentId",
                principalTable: "Course",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_Course_ParentId",
                table: "Course");

            migrationBuilder.DropIndex(
                name: "IX_Course_ParentId",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "Course");
        }
    }
}
