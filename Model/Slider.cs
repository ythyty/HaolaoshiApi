﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /// <summary>
    /// 幻灯片
    /// </summary>
    [Serializable]
    [Table("Slider")]
    public class Slider : SchoolUserID
    {

        [Display(Name = "标题")]
        public string Name { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        [Required(ErrorMessage = "图片必填")]
        public String Pic { get; set; }
        /// <summary>
        /// 移动端图片 
        /// </summary>
        public String M_pic { get; set; }
        /// <summary>
        /// 链接URL
        /// </summary>
        public String Url { get; set; }
        /// <summary>
        /// 点击量
        /// </summary>
        public int Click_num { get; set; }
        public String Mark { get; set; }

    }
}