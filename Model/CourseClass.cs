﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    /// <summary>
    /// 课程-教师-班级
    /// </summary>
    [Serializable]
    [Table("CourseClass")]
    public class CourseClass : SchoolUserTeacherID
    {      
        [Required(ErrorMessage = "班级编号必填")]
        public int? ClasssId { get; set; }
        [ForeignKey("ClasssId")]
        [Display(Name = "班级")] 
        public virtual Classs Classs { get; set; }

        //[Required(ErrorMessage = "教师编号必填")]
        //public int? TeacherId { get; set; }
        //[ForeignKey("TeacherId")]
        //public virtual Teacher Teacher { get; set; }

        [Required(ErrorMessage = "课程编号必填")]      
        public int? CourseId { get; set; }
        [ForeignKey("CourseId")]  
        [Display(Name = "课程")]
        public virtual Course Course { get; set; }
        [Display(Name = "职位")]
        public string Post { get; set; }
    }
}
