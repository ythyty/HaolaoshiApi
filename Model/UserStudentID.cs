﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model
{
    /// <summary>
    /// 统一
    /// </summary>
   public class UserStudentID: UserID,IStudent
    {       
        //public int? UserId { get; set; }
        //[ForeignKey("UserId")]
        //public virtual User User { get; set; }      
        public int? StudentId { get; set; }
        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }
    }
}
