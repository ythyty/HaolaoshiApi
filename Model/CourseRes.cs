﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Common.Util;

namespace Model
{
    /// <summary>
    /// 课程资源 如段子 笑话  案例 教案 讲义等 主要供老师分享，学生也可提供案例等素材
    /// </summary>
    [Serializable]
    [Table("CourseRes")]
    public class CourseRes : SchoolUserStudentTeacherID
    {    
        [Display(Name = "标题")]
        public string Name { get; set; }
        /// <summary>
        /// 多个用“;”隔开
        /// </summary>
        [Display(Name = "封面")]
        public string Pic { get; set; }       
        public CourseResType Type { get; set; }//课程资源类型
        [Display(Name = "简介")]
        public string Intro { get; set; }
        [Display(Name = "内容")]
        public string Detail { get; set; }
        /// <summary>
        /// 附件，多个用“;”隔开
        /// </summary>
        [Display(Name = "附件")]
        public string Attach { get; set; }
        /// <summary>
        /// 标签，多个用“;”隔开
        /// </summary>
        [Display(Name = "标签")]
        public string Tag { get; set; }
        public int? CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }           
       
        //public int? TeacherId { get; set; }
        //[ForeignKey("TeacherId")]
        //public virtual Teacher Teacher { get; set; }
        //public int? StudentId { get; set; }
        //[ForeignKey("StudentId")]
        //public virtual Student Student { get; set; }
        
    }
    /// <summary>
    /// 课程资源类型：笑话段子、精彩故事、趣味游戏、生动案例、智力思考、教案讲义
    /// </summary>
    public enum CourseResType
    {
        [Display(Name = "笑话段子")]
        joke = 1,
        [Display(Name = "精彩故事")]
        story = 2,
        [Display(Name = "趣味游戏")]
        game = 3,
        [Display(Name = "生动案例")]
        example = 4,
        [Display(Name = "智力思考")]
        mind = 5,
        [Display(Name = "教案讲义")]
        lesson_plan = 6
    }

}