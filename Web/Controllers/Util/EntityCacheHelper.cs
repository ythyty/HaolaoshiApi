﻿using Bll;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Redis;
using Web.Util;

namespace Web.Controllers.Util
{
    /// <summary>
    /// 实体缓存帮助类
    /// </summary>
    public class EntityCacheHelper
    {
        private static EntityCacheHelper instance;
        public static EntityCacheHelper GetInstance()
        {
            if (instance == null)
            {
                instance = new EntityCacheHelper();
            }
            return instance;
        }
        public void Put(SchoolConfig obj)
        {
            MyRedisHelper redis = MyRedisHelper.Instance();
            redis.StringSet<SchoolConfig>("entity_" + obj.SchoolId, obj);
        }
        public SchoolConfig GetSchoolConfig(int schoolId)
        {
            ISchoolConfigBll schoolConfigBll = ConfigHelper.GetService<ISchoolConfigBll>();
            MyRedisHelper redis = MyRedisHelper.Instance();
            SchoolConfig obj = redis.StringGet<SchoolConfig>("entity_" + schoolId);
            if (obj == null)
            {
                obj = schoolConfigBll.SelectOneBySchoolOrInit(schoolId);
                Put(obj);
            }
            return obj;
        }
    }
}
