﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Common.Util;
using Web.Filter;

namespace Web.Controllers
{
 
    [Route("api/[controller]/[action]")]
    [ApiController]
    [QueryFilter]
    public class CourseResController : MyBaseController<CourseRes>
    {
        ICourseResBll bll;
        public CourseResController(ICourseResBll bll)
        {
            this.bll = bll;
        }
        // GET: api/List/CourseRes
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            //return Result.Success("succeed").SetData(bll.SelectAll(o => true, pageNo, pageSize));
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/CourseRes/Get/5
        [HttpGet("{id}")]
        public Result Get(int id)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(id));
        }
 
    }
}
