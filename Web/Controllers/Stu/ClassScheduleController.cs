﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Common.Util;
using Web.Redis;
using Web.Filter;

namespace Web.Controllers.Stu
{

    [Route("api/stu/[controller]/[action]")]
    [ApiController]
    [Authorize("student")]
    [QueryFilter]
    public class ClassScheduleController : MyBaseController<ClassSchedule>
    {
        IClassScheduleBll bll;
        public ClassScheduleController(IClassScheduleBll bll)
        {
            this.bll = bll;
        }
        /// <summary>
        /// 获取今天的课程安排表
        /// </summary>
        /// <returns></returns>     
        [HttpPost]
        public Result Today()
        {
            //MyRedisHelper rd = MyRedisHelper.Instance();
            //rd.SetSysCustomKey("clock_stu");//已签到的学生集合                      
            return Result.Success("succeed").SetData(bll.SelectAll(o => o.Date == DateTime.Now.Date && o.ClasssId == Convert.ToInt32(MyUser.Project)));
        }
        /// <summary>
        /// 获取一周的课程安排表
        /// </summary>
        /// <returns></returns>     

        [HttpPost]
        public Result Week(DateTime starttime, DateTime endtime)
        {
            var db = bll.DbContext();
            var query = from c in db.ClassSchedules where c.ClasssId==MyUser.ClasssId&&  c.Date >=starttime &&c.Date<=endtime
                        //orderby p.CompanyId descending, p.Added_time descending
                        select new
                        {
                            time = c.TimeTable!=null? new DateTime(c.TimeTable.From_time.Ticks).ToString("HH:mm")+"-"+ new DateTime(c.TimeTable.To_time.Ticks).ToString("HH:mm") : "",
                            courseName = c.Course.Name,
                            className = c.Classroom.Name,
                            date=c.Date.ToString("yyyy-MM-dd"),
                            timename=c.TimeTable.Name,
                        };
            return Result.Success("succeed").SetData(query.ToList());
        }

    }
}
