using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Generic;
using static System.Linq.Expressions.Expression;

namespace DAL.Extension
{
    /// <summary>
    /// EFCore扩展Select方法(根据实体定制查询语句) 
    /// https://www.cnblogs.com/castyuan/p/10186619.html
    /// </summary>
    public static class QueryableExtentions
    {
        public static IQueryable<TResult> Select<TResult>(this IQueryable<object> query)
        {
            return Queryable.Select(query, GetLamda<object, TResult>(query.GetType().GetGenericArguments()[0]));
        }
        public static IQueryable<TResult> Select<TResult>(this IQueryable<object> query,string excludes)
        {
            return Queryable.Select(query, GetLamda<object, TResult>(query.GetType().GetGenericArguments()[0], excludes));
        }

        public static IQueryable<TResult> Select<TSource, TResult>(this IQueryable<TSource> query, string excludes)
        {
            return Queryable.Select(query, GetLamda<TSource, TResult>(null, excludes));
        }        
        public static Expression<Func<TSource, TResult>> GetLamda<TSource, TResult>(Type type = null, string excludes=null)
        {
            var sourceType = typeof(TSource);
            var resultType = typeof(TResult);
            var parameter = Parameter(sourceType);
            Expression propertyParameter;
            if (type != null)
            {
                propertyParameter = Convert(parameter, type);
                sourceType = type;
            }
            else
                propertyParameter = parameter;

            return Lambda<Func<TSource, TResult>>(GetExpression(propertyParameter, sourceType, resultType, excludes), parameter);
        }

        public static MemberInitExpression GetExpression(Expression parameter, Type sourceType, Type resultType, string excludes = null)
        {
            var memberBindings = new List<MemberBinding>();
            //循环要查询的字段
            foreach (var resultItem in resultType.GetProperties().Where(x => x.CanWrite))
            {
                //先排除哪些字段
                if (excludes != null&& excludes.ToLower().Contains(resultItem.Name.ToLower())) continue;
                //关联
                var fromEntityAttr = resultItem.GetCustomAttribute<FromModelAttribute>();
                if (fromEntityAttr != null)
                {
                    var property = GetFromEntityExpression(parameter, sourceType, fromEntityAttr);
                    if (property != null)
                        memberBindings.Add(Bind(resultItem, property));
                    continue;
                }

                var sourceItem = sourceType.GetProperty(resultItem.Name);
                if (sourceItem == null)//当没有对应的属性时，查找 实体名+属性
                {
                    var complexSourceItemProperty = GetCombinationExpression(parameter, sourceType, resultItem);
                    if (complexSourceItemProperty != null)
                        memberBindings.Add(Bind(resultItem, complexSourceItemProperty));
                    continue;
                }

                //判断实体的读写权限
                if (sourceItem == null || !sourceItem.CanRead)
                    continue;

                //标注NotMapped特性的属性忽略转换
                if (sourceItem.GetCustomAttribute<NotMappedAttribute>() != null)
                    continue;

                var sourceProperty = Property(parameter, sourceItem);

                //当非值类型且类型不相同时
                if (!sourceItem.PropertyType.IsValueType && sourceItem.PropertyType != resultItem.PropertyType && resultItem.PropertyType != resultType)
                {
                    //判断都是(非泛型、非数组)class
                    if (sourceItem.PropertyType.IsClass && resultItem.PropertyType.IsClass
                        && !sourceItem.PropertyType.IsArray && !resultItem.PropertyType.IsArray
                        && !sourceItem.PropertyType.IsGenericType && !resultItem.PropertyType.IsGenericType)
                    {
                        var expression = GetExpression(sourceProperty, sourceItem.PropertyType, resultItem.PropertyType);
                        memberBindings.Add(Bind(resultItem, expression));
                    }
                    continue;
                }

                if (resultItem.PropertyType != sourceItem.PropertyType)
                    continue;

                memberBindings.Add(Bind(resultItem, sourceProperty));
            }

            return MemberInit(New(resultType), memberBindings);
        }

        /// <summary>
        /// 根据FromModelAttribute 的值获取属性对应的路径
        /// </summary>
        /// <param name="sourceProperty"></param>
        /// <param name="sourceType"></param>
        /// <param name="FromModelAttribute"></param>
        /// <returns></returns>
        private static Expression GetFromEntityExpression(Expression sourceProperty, Type sourceType, FromModelAttribute FromModelAttribute)
        {
            var findType = sourceType;
            var resultProperty = sourceProperty;
            var tableNames = FromModelAttribute.ModelNames;
            if (tableNames == null)
            {
                var columnProperty = findType.GetProperty(FromModelAttribute.ModelColumn);
                if (columnProperty == null)
                    return null;
                else
                    return Property(resultProperty, columnProperty);
            }

            for (int i = 0; i < tableNames.Length; i++)
            {
                var tableProperty = findType.GetProperty(tableNames[i]);
                if (tableProperty == null)
                    return null;

                findType = tableProperty.PropertyType;
                resultProperty = Property(resultProperty, tableProperty);
            }

            var property = findType.GetProperty(FromModelAttribute.ModelColumn);
            if (property == null)
                return null;
            else
                return Property(resultProperty, property);
        }

        /// <summary>
        /// 根据组合字段获取其属性路径
        /// </summary>
        /// <param name="sourceProperty"></param>
        /// <param name="sourcePropertys"></param>
        /// <param name="resultItem"></param>
        /// <returns></returns>
        private static Expression GetCombinationExpression(Expression sourceProperty, Type sourceType, PropertyInfo resultItem)
        {
            foreach (var item in sourceType.GetProperties().Where(x => x.CanRead))
            {
                if (resultItem.Name.StartsWith(item.Name))
                {
                    if (item != null && item.CanRead && item.PropertyType.IsClass && !item.PropertyType.IsGenericType)
                    {
                        var rightName = resultItem.Name.Substring(item.Name.Length);

                        var complexSourceItem = item.PropertyType.GetProperty(rightName);
                        if (complexSourceItem != null && complexSourceItem.CanRead)
                            return Property(Property(sourceProperty, item), complexSourceItem);
                    }
                }
            }

            return null;
        }
    }

    /// <summary>
    /// https://www.cnblogs.com/castyuan/p/10186619.html
    /// 用于标注字段 来自哪个表的的哪一列(仅限于有关联的表中)
    /// </summary>
    public class FromModelAttribute : Attribute
    {
        /// <summary>
        /// 类名(表名)
        /// </summary>
        public string[] ModelNames { get; }

        /// <summary>
        /// 字段(列名)
        /// </summary>
        public string ModelColumn { get; }

        /// <summary>
        /// 示例：实体类名1（表1）.实体类名2（表2）.实体类名3（表3）....字段名（列名）
        /// </summary>
        public FromModelAttribute(string entityName)
        {
            if (string.IsNullOrWhiteSpace(entityName))
            {
                throw new ArgumentException();
            }
            var names = entityName.Split('.');
            ModelColumn = names[names.Length - 1];
            if (names.Length > 1)
            {
                ModelNames = new string[names.Length - 1];
                for (int i = 0; i < names.Length - 1; i++)
                {
                    ModelNames[i] = names[i];
                }
            }
        }
        /// <summary>
        /// 列名 + 该列所在的表名 + 该列所在的表的上一级表名+……
        /// </summary>
        /// <param name="entityColuum"></param>
        /// <param name="entityNames"></param>
        public FromModelAttribute(string entityColuum, params string[] entityNames)
        {
            ModelNames = entityNames;
            ModelColumn = entityColuum;
        }
    }
}
