/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class NoticeDAL : BaseDAL<Notice>, INoticeDAL
    {
        public NoticeDAL(MyDbContext db) : base(db)
        {
        }
    }
}
