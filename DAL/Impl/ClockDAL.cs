using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class ClockDAL : BaseDAL<Clock>, IClockDAL
    {
        public ClockDAL(MyDbContext db) : base(db)
        {
        }
    }
}
