using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class SchoolConfigDAL : BaseDAL<SchoolConfig>, ISchoolConfigDAL
    {
        public SchoolConfigDAL(MyDbContext db) : base(db)
        {
        }
    }
}
